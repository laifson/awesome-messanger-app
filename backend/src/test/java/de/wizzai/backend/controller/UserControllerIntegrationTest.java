package de.wizzai.backend.controller;

import de.wizzai.backend.enums.UserStatus;
import de.wizzai.backend.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest
public class UserControllerIntegrationTest {

    @Autowired
    private UserController userController;

    @Test
    public void getAllUsersTest() {
        List<User> userList = userController.getAllUsers();

        Assertions.assertEquals(5, userList.size(), "Expected a list size of 5");

        userList.forEach(this::assertUser);
    }

    @Test
    public void getAllUsersCountTest() {
        Long existingUsers = userController.getAllUsersCount();
        Assertions.assertEquals(existingUsers, 5, "Expected a list size of 5");
    }

    @Test
    public void getByIdTest() {
        Long userId = 1L;
        ResponseEntity<User> response = userController.getById(userId);
        User user = response.getBody();

        Assertions.assertNotNull(user, "User should not be null");
        assertUser(user);

        Assertions.assertEquals(userId, user.getId(), "Expected user ID to match");
        Assertions.assertEquals("Hannah Arendt", user.getName(), "Expected user name to match");
        Assertions.assertEquals("hannibae_02@gmx.de", user.getEmail(), "Expected user email to match");
        Assertions.assertEquals(UserStatus.ACTIVE, user.getStatus(), "Expected user status to be ACTIVE");
    }

    private void assertUser(User user) {
        Assertions.assertNotNull(user.getId(), "User ID should not be null");
        Assertions.assertNotNull(user.getName(), "User name should not be null");
        Assertions.assertNotNull(user.getEmail(), "User email should not be null");
        Assertions.assertNotNull(user.getStatus(), "User status should not be null");
    }
}
