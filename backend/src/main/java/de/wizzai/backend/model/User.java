package de.wizzai.backend.model;

import de.wizzai.backend.enums.UserStatus;
import jakarta.persistence.*;
import lombok.Data;

@Entity(name = "app_user")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String email;

    @Enumerated(EnumType.STRING)
    private UserStatus status;
}
