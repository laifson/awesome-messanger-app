package de.wizzai.backend.service;

import de.wizzai.backend.model.User;
import de.wizzai.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Long getAllUsersCount() {
        return userRepository.count();
    }

    public Optional<User> getById(Long userId) {
        return userRepository.findById(userId);
    }

    //TODO: write methods for add, update and delete
}
