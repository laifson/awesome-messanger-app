package de.wizzai.backend.service;

import de.wizzai.backend.model.Message;
import de.wizzai.backend.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;

    public List<Message> getByUserId(Long userId) {
        return messageRepository.findByUserId(userId);
    }

    public int getByUserIdCount(Long userId) {
        return messageRepository.countByUserId(userId);
    }

    public Optional<Message> getById(Long messageId) {
        return messageRepository.findById(messageId);
    }

    //TODO: write methods for add, update and delete
}
