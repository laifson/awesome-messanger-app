package de.wizzai.backend.dataloader;

import de.wizzai.backend.enums.UserStatus;
import de.wizzai.backend.model.Message;
import de.wizzai.backend.model.User;
import de.wizzai.backend.repository.MessageRepository;
import de.wizzai.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component
public class DataLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;

    @Autowired
    public DataLoader(UserRepository userRepository, MessageRepository messageRepository) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }

    @Override
    public void run(String... args) {

        // demo users
        User firstUser = new User();
        firstUser.setName("Hannah Arendt");
        firstUser.setEmail("hannibae_02@gmx.de");
        firstUser.setStatus(UserStatus.ACTIVE);

        User secondUser = new User();
        secondUser.setName("Lisa Boeckler");
        secondUser.setEmail("sailormoon@hotmail.de");
        secondUser.setStatus(UserStatus.ACTIVE);

        User thirdUser = new User();
        thirdUser.setName("Dirk Frede");
        thirdUser.setEmail("metalking91@gmx.de");
        thirdUser.setStatus(UserStatus.ACTIVE);

        User fourthUser = new User();
        fourthUser.setName("Hansi Flick");
        fourthUser.setEmail("hans_mueller@gmail.de");
        fourthUser.setStatus(UserStatus.ACTIVE);

        User fifthUser = new User();
        fifthUser.setName("Andrea Pirlo");
        fifthUser.setEmail("pirlo@googlemail.com");
        fifthUser.setStatus(UserStatus.INACTIVE);

        List<User> userList = List.of(firstUser, secondUser, thirdUser, fourthUser, fifthUser);

        userRepository.saveAll(userList);

        // demo messages
        userList.forEach(this::generateRandomMessages);
    }

    private void generateRandomMessages(User user) {
        Random random = new Random();

        // Random number between 4 and 1
        int numberOfMessages = random.nextInt(4) + 1;

        for (int i = 0; i < numberOfMessages; i++) {
            Message message = new Message();
            message.setUserId(user.getId());
            message.setTitle(generateRandomText());

            messageRepository.save(message);
        }
    }

    private String generateRandomText() {
        String[] words = {"Lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit"};

        Random random = new Random();

        // Random number of words between 10 and 1
        int numberOfWords = random.nextInt(10) + 1;

        StringBuilder text = new StringBuilder();

        for (int i = 0; i < numberOfWords; i++) {
            int randomIndex = random.nextInt(words.length);
            text.append(words[randomIndex]).append(" ");
        }

        return text.toString().trim();
    }
}
