package de.wizzai.backend.enums;

public enum UserStatus {
    ACTIVE,
    INACTIVE
}
