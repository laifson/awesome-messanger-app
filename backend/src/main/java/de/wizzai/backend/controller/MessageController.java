package de.wizzai.backend.controller;

import de.wizzai.backend.model.Message;
import de.wizzai.backend.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/posts")
public class MessageController {
    @Autowired
    private MessageService messageService;

    // GET message(s) by user id
    @GetMapping("/user/{userId}")
    public List<Message> getByUserId(@PathVariable Long userId) {
        return messageService.getByUserId(userId);
    }

    @GetMapping("/user/{userId}/count")
    public int getByUserIdCount(@PathVariable Long userId) { return messageService.getByUserIdCount(userId); }

    // GET message by id
    @GetMapping("/{messageId}")
    public ResponseEntity<Message> getMessageById(@PathVariable Long messageId) {
        Optional<Message> message = messageService.getById(messageId);
        return message.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    //TODO: Add methods for create, update and delete
}
