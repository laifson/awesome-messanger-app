import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'avatarInitials'
})
export class AvatarInitialsPipe implements PipeTransform {
  transform(userName: string): string {
    // Split the name into words
    const words = userName.split(' ');

    // Initialize initials variable
    let initials = '';

    // Iterate through words and add the first letter of each word to initials
    for (const word of words) {
      if (word.length > 0) {
        initials += word[0];
      }
    }

    // Convert initials to uppercase
    initials = initials.toUpperCase();

    return initials;
  }
}
