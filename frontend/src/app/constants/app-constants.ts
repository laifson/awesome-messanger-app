export class AppConstants {
  public static readonly CARD_MESSAGE_COUNT_SUFFIX_SINGULAR = " post";
  public static readonly CARD_MESSAGE_COUNT_SUFFIX_PLURAL = " posts";
}

