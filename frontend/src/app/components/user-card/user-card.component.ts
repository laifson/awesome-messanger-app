import {Component, Input} from '@angular/core';
import {User} from "../../models/user.model";
import {MessageService} from '../../services/message.service';
import {AppConstants} from "../../constants/app-constants";

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent {
  @Input() user!: User;
  messageCount: number = 0;
  protected readonly appConstants = AppConstants;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.messageService.getByUserIdCount(this.user.id).subscribe(messagesCount => {
      this.messageCount = messagesCount;
    });
  }

  getSuffix(messageCount: number): string {
    return messageCount === 1
      ? this.appConstants.CARD_MESSAGE_COUNT_SUFFIX_SINGULAR
      : this.appConstants.CARD_MESSAGE_COUNT_SUFFIX_PLURAL;
  }
}
