import {UserStatus} from "../enums/user-status.enum";

export interface User {
  id: number;
  name: string;
  email: string;
  status: UserStatus;
}
