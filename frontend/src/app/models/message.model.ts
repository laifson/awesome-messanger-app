export interface Message {
  id: number;
  userId: number;
  title: string;
}
