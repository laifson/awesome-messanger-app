import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Message} from "../models/message.model";

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) {
  }

  getById(id: number): Observable<Message> {
    return this.http.get<Message>(`${this.baseUrl}/posts/`);
  }

  getByUserId(userId: number): Observable<Message[]> {
    return this.http.get<Message[]>(`${this.baseUrl}/posts/user/${userId}`);
  }

  getByUserIdCount(userId: number): Observable<number> {
    return this.http.get<number>(`${this.baseUrl}/posts/user/${userId}/count`);
  }
}
